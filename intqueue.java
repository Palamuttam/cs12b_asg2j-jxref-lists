//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: intqueue.java,v 1.3 2013-10-20 17:54:46-07 - - $

import java.util.Iterator;
import java.util.NoSuchElementException;

class intqueue implements Iterable<Integer> {

   private class node {
     int linenr;
     node link;
   }
   private int count = 0;
   private node front = null;
   private node rear = null;

   // Inserts a number into the linked list 
   public void insert (int number) {
     ++count;
     node next = new node();
     next.linenr = number;
     // if the head is null
     if(front == null){
        front = next;
        rear = next;
     } else {
        rear.link = next;
        rear = next;
     }
   }

   public boolean empty() {
     return count == 0;
   }

   public int getcount() {
     return count;
   }

   public Iterator<Integer> iterator() {
     return new iterator();
   }

   private class iterator implements Iterator<Integer> {
     node curr = front;

     public boolean hasNext() {
       return curr != null;
     }

     public Integer next() {
       if (curr == null) throw new NoSuchElementException();
       Integer next = curr.linenr;
       curr = curr.link;
       return next;
     }

     public void remove() {
       throw new UnsupportedOperationException();
     }
   }

}

