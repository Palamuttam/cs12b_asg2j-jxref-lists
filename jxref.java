//Author:x Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: jxref.java,v 1.15 2013-10-20 17:54:46-07 - - $

//NAME
//   jxref -- cross referencing and word count utility

// SYNOPSIS
//   jxref [filename ...]

//DESCRIPTION
//   Each file is read in sequence and words are extracted
//   from the file. At the end of each file, a table
//   is printed, sorted in lexicographic order with 
//   each word followed by a count of the number of times 
//   it occurs and a list of the numbers of the lines where
//   it occurs. Cross reference output is written to stdout
//   (System.out) and error messages are written to stderr
//   (System.err).

//OPERANDS
//   Operands consist of the names of files to be read in
//   sequence. If no filenames are specified, stdin 
//   is read. If filenames are specified, each file is
//   read in turn. If a filename is specified as a single
//   minus sign (-) stdin is read at that point. In order
//   to read a file whose name really is a minus sign,
//   it should be specified on the comman line as "./-".

//EXIT STATUS
//   0   All input files were read successfully
//   1   Error(s) occurred and message(s) was printed to stderr

import java.io.*;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.lang.System.*;

class jxref {
   private static final String STDIN_FILENAME = "-";
   private static final String REGEX = "\\w+([-'.:/]\\w+)*";
   private static final Pattern PATTERN = Pattern.compile(REGEX);

   private static void xref_file (String filename, Scanner file){
       // Prints the heading
      String head = "::::::::::::::::::::::::::::::::";
      out.printf("%s%n%s%n%1$s%n", head, filename);
      listmap map = new listmap();
      // Parses through the lines in filename
      for (int linenr = 1; file.hasNextLine(); ++linenr) {
         String line = file.nextLine();
         Matcher match = PATTERN.matcher (line);
         while (match.find()) {
            String word = match.group();
            //inserts the word into the list
            map.insert(word, linenr);
         }
      }
      // Prints out the data in each node in the listmap
      for (Entry<String, intqueue> entry: map) {
         String lines = "";
         //lists all the line numbers the word occurs in
         for (Integer next : entry.getValue()) lines += " " +  next;
         out.format("%s [%d]%s%n", entry.getKey() ,
                     entry.getValue().getcount(), lines);
      }
   }

   //  Opens the specified file, cross references it, and prints.
   private static void xref_filename (String filename) {
      // check to see if "-" was passed as an arg
      if (filename.equals (STDIN_FILENAME)) {
         xref_file (filename, new Scanner (System.in));
      }else {
         // catches non-existent file names
         try {
            Scanner file = new Scanner (new File (filename));
            xref_file (filename, file);
         file.close();
       }catch (IOException error) {
         misc.warn (error.getMessage());
       }
     }
   }

   // Main function scans arguments to cross reference files.
   public static void main (String[] args) {
     if (args.length == 0) {
       xref_filename (STDIN_FILENAME);
     }else {
       for (String filename: args) {
         xref_filename (filename);
       }
     }
     exit (misc.exit_status);
   }
}

