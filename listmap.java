//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: listmap.java,v 1.4 2013-10-20 17:54:46-07 - - $

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import static java.lang.System.*;

class listmap implements Iterable<Entry<String,intqueue>> {

   private class node implements Entry<String,intqueue> {
      String key;
      intqueue queue = new intqueue();
      node link;
      public String getKey() {
         return key;
      }
      public intqueue getValue() {
         return queue;
      }
      public intqueue setValue (intqueue queue) {
         throw new UnsupportedOperationException();
      }
   }
   private node head = null;

   public listmap() {
      // Not needed, since head defaults to null anyway.
   }

   /* Searches down the list using a previous and current pointer,
    *  and inserts a new key in ascending lexicographic order.
    *  If the word is not in the list a new node is allocated and 
    *  inserted in the correct position.
   */
   public void insert (String key, int linenr) {
      // current and previous pointers
      node prev = null;
      node curr = head;
      // Phase 1: 
      //Searches for the position to insert
      while (curr != null){
         //break out if a "higher" word has been found
         if(key.compareTo(curr.key) < 0) break;
         //If the word is found, add to the queue and stop
         if(key.compareTo(curr.key) == 0){
            curr.queue.insert(linenr);
            return;
         } 
         prev = curr;
         curr = curr.link;
      }
         // Phase 2:
         // Allocates a new node containing the key and inserts it
         node next = new node(); 
         next.key = key;
         // the next node points to the current node
         next.link = curr;
         next.queue.insert(linenr);
         if (prev == null){
            head = next;
         } else {
            prev.link = next;
         }
   }

   public Iterator<Entry<String,intqueue>> iterator() {
      return new iterator();
   }

   private class iterator
      implements Iterator<Entry<String,intqueue>> {
      node curr = head;

      public boolean hasNext() {
         return curr != null;
      }

      public Entry<String,intqueue> next() {
         if (curr == null) throw new NoSuchElementException();
         node next = curr;
         curr = curr.link;
         return next;
      }

      public void remove() {
         throw new UnsupportedOperationException();
      }
   }
}
